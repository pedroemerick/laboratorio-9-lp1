/**
 * @file	insertionsort.h
 * @brief	Implementacao da funcao de ordenacao insertionsort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Insertion Sort
     * @param 	vetor Vetor para ordenacao
     * @param	tamanho Tamanho do vetor
     */
    template < typename T >
    void insertionsort (T *vetor, int tamanho)
    {
        for (int ii = 1; ii < tamanho; ii++)
        {
            int aux = ii;

            while (aux > 0 && (vetor[aux - 1] > vetor [aux]))
            {
                T temp = vetor[aux];
                vetor[aux] = vetor[aux - 1];
                vetor[aux - 1] = temp;
                
                aux--;
            }
        }
    }
}

#endif