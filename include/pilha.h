/**
 * @file	pilha.h
 * @brief	Definicao e implementacao da classe Pilha, que representa um tipo abstrato de dado chamado
 *          pilha/stack
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/06/2017
 * @date	17/06/2017
 */

#ifndef PILHA_H
#define PILHA_H

#include "_node.h"
using namespace edb1;

namespace edb1
{
    /** 
    * @class 	Pilha pilha.h
    * @brief 	Classe que representa um instante de estrutura Pilha
    * @details  Os atributos de um instante de Pilha sao um apontador para a base da Pilha
    *           e um apontador para o topo da Pilha
    */
    template < typename T >
    class Pilha {
        private:
            Node <T> *base;                    /**< Apontador para a base da pilha */ 
            Node <T> *topo;                    /**< Apontador para o topo da pilha */
        public:
            /** @brief Construtor padrao */
            Pilha ();

            /** @brief Inseri um elemento na pilha */
            void Push (T el);

            /** @brief Remove um elemento da pilha */
            void Pop ();

            /** @brief Retorna o elemento que esta no topo da pilha */
            T Top ();

            /** @brief Retorna se a pilha está vazia ou não */
            bool Empty ();

            /** @brief Retorna o tamanho da pilha */
            int Tamanho ();

            /** @brief Destrutor padrão */
            ~Pilha ();
    };

    /**
     * @details Os apontadores inicio e fim sao iniciados com o vazio
     */
    template < typename T >
    Pilha<T>::Pilha () {
        base = NULL;
        topo = NULL;
    }

    /**
     * @details O metodo generico inseri um elemento no topo da pilha
     * @param   el Novo elemento para a inserir na pilha 
     */
    template < typename T >
    void Pilha<T>::Push (T el) {
        Node <T> *novo = new Node <T>;
        novo->setDado (el);

        if (base == NULL)
        {
            base = novo;
            topo = novo;
        }

        topo->setProx (novo);
        novo->setAnt (topo);
        novo->setProx (NULL);
        topo = novo;
    }

    /**
     * @details O metodo generico remove o elemento que esta no topo da pilha
     */
    template < typename T >
    void Pilha<T>::Pop () {
        Node <T> *aux = topo;

        if (aux->getAnt () == NULL && aux->getProx () == NULL)
        {
            topo = NULL;

            delete aux;
        }
        else
        {
            aux->getAnt()->setProx (NULL);
            topo = aux->getAnt ();

            delete aux;
        }

        // aux->getAnt()->setProx (NULL);
        // topo = aux->getAnt ();

        // delete aux;
    }

    /**
     * @return O elemento que esta no topo da pilha
     */
    template < typename T >
    T Pilha<T>::Top () {
        return topo->getDado ();
    }

    /**
     * @return Se a pilha está vazia ou não
     */
    template < typename T >
    bool Pilha<T>::Empty () {
        if (topo == NULL)
            return true;
        else 
            return false;
    }

    /**
     * @return Numero de elementos/tamanho da pilha
     */
    template < typename T >
    int Pilha<T>::Tamanho () {
        Node <T> *aux = base;
        int size = 0;

        while (aux != NULL)
        {
            size += 1;
            aux = aux->getProx ();
        }

        return size;
    }

    /**
     * @details É liberado todos os elementos da pilha se alocado
     */
    template < typename T >
    Pilha<T>::~Pilha () {
        
        while (base != NULL)
        {
            Node <T> *aux = base;
            base = base->getProx ();

            delete aux;
        }
    }
}

#endif