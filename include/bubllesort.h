/**
 * @file	bubllesort.h
 * @brief	Implementacao da funcao de ordenacao bubllesort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef BUBLLESORT_H
#define BUBLLESORT_H

namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Bublle Sort
     * @param 	vetor Vetor para ordenacao
     * @param	tamanho Tamanho do vetor
     */
    template < typename T >
    void bubllesort (T *vetor, int tamanho)
    {
        for (int ii = 0; ii < (tamanho - 1); ii++)
        {
            for (int jj = 0; jj < (tamanho - ii - 1); jj++)
            {
                if (vetor[jj] > vetor[jj+1])
                {
                    T temp = vetor[jj + 1];
                    vetor[jj + 1] = vetor[jj];
                    vetor[jj] = temp;
                }
            }
        }
    }
}

#endif