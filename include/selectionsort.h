/**
 * @file	selectionsort.h
 * @brief	Implementacao da funcao de ordenacao selectionsort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef SELECTIONSORT_H
#define SELECTIONSORT_H

namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Selection Sort
     * @param 	vetor Vetor para ordenacao
     * @param	tamanho Tamanho do vetor
     */
    template < typename T >
    void selectionsort (T *vetor, int tamanho)
    {
        for (int ii = 0; ii < tamanho-1; ii++)
        {
            int menor = ii;

            for (int jj = ii+1; jj < tamanho; jj++)
            {
                if (vetor[jj] < vetor[menor])
                {
                    menor = jj;
                }
            }

            if (menor != ii)
            {
                T aux = vetor[ii];
                vetor[ii] = vetor[menor];
                vetor[menor] = aux;
            }
        }
    }
}

#endif