/**
* @file     teste_tads.h
* @brief    Declaracao dos prototipos das funcoes que fazem os testes com os tipos abstratos de dados
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#ifndef TESTES_TADS_H
#define TESTES_TADS_H

/** 
 * @brief	Função que realiza testes no tad lista, implementados na biblioteca
 */
void teste_lista ();

/** 
 * @brief	Função que realiza testes no tad fila, implementados na biblioteca
 */
void teste_fila ();

/** 
 * @brief	Função que realiza testes no tad pilha, implementados na biblioteca
 */
void teste_pilha ();

/** 
 * @brief	Função que chama as funcoes que fazem os testes nos TADs
 */
void testes_tads ();

#endif