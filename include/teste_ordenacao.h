/**
* @file     teste_ordenacao.h
* @brief    Declaracao do prototipo da funcao que faz os testes com os algoritmos de ordenacao
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#ifndef TESTE_ORDENACAO_H
#define TESTE_ORDENACAO_H

/** 
 * @brief	Função que realiza testes nos algoritmos de ordenacao, implementados na biblioteca
 */
void testes_ordenacao ();

#endif