/**
* @file     funcoes_vetor.h
* @brief    Declaracao dos prototipos e implementacao das funcoes que manipulam um vetor
* @author   Pedro Emerick (p.emerick@live.com)
* @since    20/06/2017
* @date	    20/06/2017
*/

#ifndef FUNCOES_VETOR_H
#define FUNCOES_VETOR_H

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

/** 
 * @brief	Função generica que faz a impressao de um vetor
 * @param 	vetor Vetor para impressao
 * @param	tamanho Tamanho do vetor
 */
template < typename T >
void imprimi_vetor (T *vetor, int tamanho)
{
    for (int ii = 0; ii < tamanho; ii++)
    {
        cout << vetor[ii] << " ";
    }
    cout << endl;
}

/** 
 * @brief	Função que preenche um vetor de inteiros com valores aleatorios de zero a cem
 * @param 	vetor Vetor para inserir os numeros
 * @param	tamanho Tamanho do vetor
 */
void vetor_aleatorio_int (int *vetor, int tamanho);

/** 
 * @brief	Função que preenche um vetor de floats com valores aleatorios de zero a cem
 * @param 	vetor Vetor para inserir os numeros
 * @param	tamanho Tamanho do vetor
 */
void vetor_aleatorio_float (float *vetor, int tamanho);

/** 
 * @brief	Função que preenche um vetor de strings com valores aleatorios de zero a cem
 * @param 	vetor Vetor para inserir as strings
 * @param	tamanho Tamanho do vetor
 */
void vetor_aleatorio_string (string *vetor, int tamanho);

#endif