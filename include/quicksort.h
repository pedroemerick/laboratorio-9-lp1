/**
 * @file	quicksort.h
 * @brief	Implementacao da funcao de ordenacao quicksort
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/03/2017
 * @date	20/04/2017
 */

#ifndef QUICKSORT_H
#define QUICKSORT_H

namespace edb1
{
    /** 
     * @brief   Funcao generica que faz a ordenacao de um vetor pelo metodo de Quick Sort
     * @param 	vetor Vetor para ordenacao
     * @param	inicio Indice do inicio do vetor
     * @param	fim Indice do fim do vetor
     */
    template < typename T >
    void quicksort (T *vetor, int inicio, int fim)
    {
        int aux1 = inicio;
        int aux2 = fim;

        T pivot = vetor [(inicio + fim) / 2];

        while (aux1 <= aux2)
        {
            while (vetor[aux1] < pivot)
                aux1++;
            while (vetor[aux2] > pivot)
                aux2--;
            
            if (aux1 <= aux2)
            {
                T temp = vetor[aux1];
                vetor[aux1] = vetor[aux2];
                vetor[aux2] = temp;

                aux1++;
                aux2--;
            }
        }
        
        if (inicio < aux2)
            quicksort (vetor, inicio, aux2);
        if (aux1 < fim)
            quicksort (vetor, aux1, fim);
    }
}

#endif