/**
 * @file	fila.h
 * @brief	Definicao e implementacao da classe Fila, que representa um tipo abstrato de dado chamado
 *          fila
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/06/2017
 * @date	17/06/2017
 */

#ifndef FILA_H
#define FILA_H

#include "_node.h"

using namespace edb1;

namespace edb1
{
    /** 
    * @class 	Fila fila.h
    * @brief 	Classe que representa um instante de estrutura Fila
    * @details  Os atributos de um instante de Fila sao um apontador para o inicio da Fila
    *           e um apontador para o fim da Fila
    */
    template < typename T >
    class Fila {
        private:
            Node <T> *inicio;                   /**< Apontador para o inicio da fila */ 
            Node <T> *fim;                      /**< Apontador para o fim da fila */ 
        public:
            /** @brief Construtor padrao */
            Fila ();

            /** @brief Inseri um elemento na fila */
            void Push (T el);

            /** @brief Retorna o primeiro elemento da fila */
            T Front ();

            /** @brief Retorna o ultimo elemento da fila */
            T Back ();

            /** @brief Remove um elemento da fila */
            void Pop ();

            /** @brief Retorna se a fila está vazia ou não */
            bool Empty ();

            /** @brief Retorna o tamanho da fila */
            int Tamanho ();

            /** @brief Destrutor padrão */
            ~Fila ();
    };

    /**
     * @details Os apontadores inicio e fim sao iniciados com o vazio
     */
    template < typename T >
    Fila<T>::Fila () {
        inicio = NULL;
        fim = NULL;
    }

    /**
     * @details O metodo generico inseri um elemento no fim da fila
     * @param   el Novo elemento para a inserir na fila 
     */
    template < typename T >
    void Fila<T>::Push (T el) {
        Node <T> *novo = new Node <T>;
        novo->setDado (el);

        if (inicio == NULL)
        {
            inicio = novo;
            fim = novo;
        }

        fim->setProx (novo);
        novo->setAnt (fim);
        novo->setProx (NULL);
        fim = novo;
    }

    /**
     * @return Primeiro elemento da fila
     */
    template < typename T >
    T Fila<T>::Front () {
        return inicio->getDado ();
    }

    /**
     * @return Ultimo elemento da fila
     */
    template < typename T >
    T Fila<T>::Back () {
        return fim->getDado ();
    }

    /**
     * @details O metodo generico remove o primeiro elemento da fila
     */
    template < typename T >
    void Fila<T>::Pop () {
        Node <T> *aux = inicio;

        if (aux->getAnt () == NULL && aux->getProx () == NULL)
        {
            inicio = NULL;

            delete aux;
        }
        else
        {
            aux->getProx()->setAnt (NULL);
            inicio = aux->getProx ();

            delete aux;
        }
    }

    /**
     * @return Se a fila está vazia ou não
     */
    template < typename T >
    bool Fila<T>::Empty () {
        if (inicio == NULL)
            return true;
        else 
            return false;
    }

    /**
     * @return Numero de elementos/tamanho da fila
     */
    template < typename T >
    int Fila<T>::Tamanho () {
        Node <T> *aux = inicio;
        int size = 0;

        while (aux != NULL)
        {
            size += 1;
            aux = aux->getProx ();
        }

        return size;
    }

    /**
     * @details É liberado todos os elementos da fila se alocado
     */
    template < typename T >
    Fila<T>::~Fila () {
        while (inicio != NULL)
        {
            Node <T> *aux = inicio;
            inicio = inicio->getProx ();

            delete aux;
        }
    }
}

#endif