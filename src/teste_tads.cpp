/**
* @file     teste_tads.cpp
* @brief    Implementacao das funcoes que fazem os testes com os tipos abstratos de dados
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#include "teste_tads.h"

#include "pilha.h"
#include "ll_dupla_ord.h"
#include "fila.h"

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <sstream>
using std::stringstream;

using namespace edb1;

/** 
 * @brief	Função que realiza testes no tad lista, implementados na biblioteca
 */
void teste_lista ()
{
    /** Testando a lista ligada duplamente encadeada */
    cout << "-> LISTA" << endl << endl;
    
    Lista <int> lista;
    Lista <float> lista_f;
    Lista <string> lista_s;

    /** Testando lista de inteiros */
    cout << "Inserindo 5 numeros inteiros aleatorios na lista de INTEIRO: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        int temp = rand () % 100;
        lista.Inserir (temp);
        cout << "Inserindo " << temp << " ..." << endl;
    }

    int aux;

    cout << "Elementos da Lista: ";
    for (Node <int> *ii = lista.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        cout << *(ii->getDadoPtr ()) << " ";
        aux = *(ii->getDadoPtr ());
    }
    cout << endl;
    
    cout << "Removendo elemento " << aux << " da lista ..." << endl;
    lista.Remover (aux);

    cout << "Lista apos remocao: ";
    for (Node <int> *ii = lista.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        cout << *(ii->getDadoPtr ()) << " ";
    }
    cout << endl;  

    /** Testando lista de floats */
    cout << endl;
    cout << "Inserindo 5 numeros floats aleatorios na lista de FLOAT: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        float temp = rand () % 100;
        int temp2 = rand () % 100;
        temp += (float) temp2/1000;
        lista_f.Inserir (temp);
        cout << "Inserindo " << temp << " ..." << endl;
    }

    float aux_f;

    cout << "Elementos da Lista: ";
    for (Node <float> *ii = lista_f.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        cout << *(ii->getDadoPtr ()) << " ";
        aux_f = *(ii->getDadoPtr ());
    }
    cout << endl;
    
    cout << "Removendo elemento " << aux_f << " da lista ..." << endl;
    lista_f.Remover (aux_f);

    cout << "Lista apos remocao: ";
    for (Node <float> *ii = lista_f.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        cout << *(ii->getDadoPtr ()) << " ";
    }
    cout << endl;  

    /** Testando lista de strings */
    cout << endl;
    cout << "Inserindo 5 strings aleatorias na lista de STRING: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        string temp_s = "teste";

        int temp = rand () % 30;        
        stringstream num_teste;
        num_teste << temp;

        temp_s += num_teste.str ();

        lista_s.Inserir (temp_s);
        cout << "Inserindo " << temp_s << " ..." << endl;
    }

    string aux_s;

    cout << "Elementos da Lista: ";
    for (Node <string> *ii = lista_s.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        cout << *(ii->getDadoPtr ()) << " ";
        aux_s = *(ii->getDadoPtr ());
    }
    cout << endl;
    
    cout << "Removendo elemento " << aux_s << " da lista ..." << endl;
    lista_s.Remover (aux_s);

    cout << "Lista apos remocao: ";
    for (Node <string> *ii = lista_s.getInicio (); ii != NULL; ii = ii->getProx ())
    {
        cout << *(ii->getDadoPtr ()) << " ";
    }
    cout << endl; 
}

/** 
 * @brief	Função que realiza testes no tad fila, implementados na biblioteca
 */
void teste_fila ()
{
    /** Testando a fila */
    cout << endl << "-> FILA" << endl << endl;

    Fila <int> fila;
    Fila <float> fila_f;
    Fila <string> fila_s;

    /** Testando a fila de inteiros */
    cout << "Inserindo 5 numeros inteiros aleatorios na fila de INTEIRO: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        int temp = rand () % 100;
        fila.Push (temp);
        cout << "Inserindo " << temp << " ..." << endl;
    }

    cout << "Tamanho da fila: " << fila.Tamanho () << endl;

    cout << "Verificando se a fila está vazia: ";
    if (fila.Empty () == false)
        cout << "A fila nao esta vazia" << endl;
    else    
        cout << "A fila esta vazia" << endl;
        
    cout << "Retornando o ultimo elemento da fila: " << fila.Back () << endl;

    cout << "Retornando o primeiro elemento da fila: " << fila.Front () << endl;

    cout << "Removendo o primeiro elemento da fila ..." << endl;
    fila.Pop ();

    cout << "Retornando o primeiro elemento da fila apos remocao: " << fila.Front () << endl;

    /** Testando a fila de floats */
    cout << endl;
    cout << "Inserindo 5 numeros floats aleatorios na fila de FLOAT: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        float temp = rand () % 100;
        int temp2 = rand () % 100;
        temp += (float) temp2/1000;
        fila_f.Push (temp);
        cout << "Inserindo " << temp << " ..." << endl;
    }

    cout << "Tamanho da fila: " << fila_f.Tamanho () << endl;

    cout << "Verificando se a fila está vazia: ";
    if (fila_f.Empty () == false)
        cout << "A fila nao esta vazia" << endl;
    else    
        cout << "A fila esta vazia" << endl;
        
    cout << "Retornando o ultimo elemento da fila: " << fila_f.Back () << endl;

    cout << "Retornando o primeiro elemento da fila: " << fila_f.Front () << endl;

    cout << "Removendo o primeiro elemento da fila ..." << endl;
    fila_f.Pop ();

    cout << "Retornando o primeiro elemento da fila apos remocao: " << fila_f.Front () << endl;

    /** Testando a fila de strings */
    cout << endl;
    cout << "Inserindo 5 strings aleatorias na fila de STRING: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        string temp_s = "teste";

        int temp = rand () % 30;        
        stringstream num_teste;
        num_teste << temp;

        temp_s += num_teste.str ();

        fila_s.Push (temp_s);
        cout << "Inserindo " << temp_s << " ..." << endl;
    }

    cout << "Tamanho da fila: " << fila_s.Tamanho () << endl;

    cout << "Verificando se a fila está vazia: ";
    if (fila_s.Empty () == false)
        cout << "A fila nao esta vazia" << endl;
    else    
        cout << "A fila esta vazia" << endl;
        
    cout << "Retornando o ultimo elemento da fila: " << fila_s.Back () << endl;

    cout << "Retornando o primeiro elemento da fila: " << fila_s.Front () << endl;

    cout << "Removendo o primeiro elemento da fila ..." << endl;
    fila_s.Pop ();

    cout << "Retornando o primeiro elemento da fila apos remocao: " << fila_s.Front () << endl;
}   

/** 
 * @brief	Função que realiza testes no tad pilha, implementados na biblioteca
 */
void teste_pilha ()
{
    /** Testando a pilha */
    cout << endl << "-> PILHA" << endl << endl;

    Pilha <int> pilha;
    Pilha <float> pilha_f;
    Pilha <string> pilha_s;

    /** Testando a pilha de inteiros */
    cout << "Inserindo 5 numeros inteiros aleatorios na pilha de INTEIRO: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        int temp = rand () % 100;
        pilha.Push (temp);
        cout << "Inserindo " << temp << " ..." << endl;
    }

    cout << "Tamanho da pilha: " << pilha.Tamanho () << endl;

    cout << "Verificando se a pilha está vazia: ";
    if (pilha.Empty () == false)
        cout << "A pilha nao esta vazia" << endl;
    else    
        cout << "A pilha esta vazia" << endl;
        
    cout << "Retornando o elemento do topo da pilha: " << pilha.Top () << endl;

    cout << "Removendo o elemento do topo da pilha ..." << endl;
    pilha.Pop ();

    cout << "Retornando o elemento do topo da pilha apos remocao: " << pilha.Top () << endl;

    /** Testando a pilha de floats */
    cout << endl;
    cout << "Inserindo 5 numeros inteiros aleatorios na pilha de FLOAT: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        float temp = rand () % 100;
        int temp2 = rand () % 100;
        temp += (float) temp2/1000;
        pilha_f.Push (temp);
        cout << "Inserindo " << temp << " ..." << endl;
    }

    cout << "Tamanho da pilha: " << pilha_f.Tamanho () << endl;

    cout << "Verificando se a pilha está vazia: ";
    if (pilha_f.Empty () == false)
        cout << "A pilha nao esta vazia" << endl;
    else    
        cout << "A pilha esta vazia" << endl;
        
    cout << "Retornando o elemento do topo da pilha: " << pilha_f.Top () << endl;

    cout << "Removendo o elemento do topo da pilha ..." << endl;
    pilha_f.Pop ();

    cout << "Retornando o elemento do topo da pilha apos remocao: " << pilha_f.Top () << endl;

    /** Testando a pilha de strings */
    cout << endl;
    cout << "Inserindo 5 strings aleatorias na pilha de STRING: " << endl;

    for (int ii = 0; ii < 5; ii++)
    {
        string temp_s = "teste";

        int temp = rand () % 30;        
        stringstream num_teste;
        num_teste << temp;

        temp_s += num_teste.str ();

        pilha_s.Push (temp_s);
        cout << "Inserindo " << temp_s << " ..." << endl;
    }

    cout << "Tamanho da pilha: " << pilha_s.Tamanho () << endl;

    cout << "Verificando se a pilha está vazia: ";
    if (pilha_s.Empty () == false)
        cout << "A pilha nao esta vazia" << endl;
    else    
        cout << "A pilha esta vazia" << endl;
        
    cout << "Retornando o elemento do topo da pilha: " << pilha_s.Top () << endl;

    cout << "Removendo o elemento do topo da pilha ..." << endl;
    pilha_s.Pop ();

    cout << "Retornando o elemento do topo da pilha apos remocao: " << pilha_s.Top () << endl;
}

/** 
 * @brief	Função que chama as funcoes que fazem os testes nos TADs
 */
void testes_tads ()
{
    cout << "***********************************" << endl;
    cout << "*          TESTANDO TADs          *" << endl;
    cout << "***********************************" << endl << endl;

    teste_lista ();
    teste_fila ();
    teste_pilha ();
}