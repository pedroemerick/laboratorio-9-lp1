/**
* @file     teste_busca.cpp
* @brief    Implementacao da funcao que faz os testes com os algoritmos de busca
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#include "teste_busca.h"

#include "buscas.h"
#include "quicksort.h"

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;
using std::getline;

#include <sstream> 
using std::ws;

using namespace edb1;

#include "funcoes_vetor.h"

/** 
 * @brief	Função que realiza testes nos algoritmos de busca, implementados na biblioteca
 */
void testes_busca ()
{
    cout << "O vetor usado nestes testes tem tamanho 10 com valores aleatorios";
    cout << endl << endl;

    cout << "***********************************" << endl;
    cout << "*         TESTANDO BUSCAS         *" << endl;
    cout << "***********************************" << endl << endl;

    int vetor [10];
    float vetor_f [10];
    string vetor_s [10] = {"pedro", "joao" ,"lucas", "valmir", "silvio", "gildo", "raul", "luan", "teixeira", "imd"};
    int num;
    float num_f;
    int indice_resultado;

    /** ********************************  TESTANDO INTEIRO   ************************************* */
    cout << "*************************************  TESTANDO INTEIRO  *************************************" << endl << endl;
    vetor_aleatorio_int (vetor, 10);
    
    cout << "--> BUSCA EM VETOR DE INTEIRO:" << endl << endl;
    cout << "Vetor INTEIRO para busca: ";
    imprimi_vetor (vetor, 10);
    cout << endl;

    cout << "Digite um numero para verificar se existe no vetor atraves das funcoes de busca: ";
    cin >> num;

    /** Testando a busca sequencial iterativa */
    cout << endl << "-> BUSCA SEQUENCIAL ITERATIVA: ";
    indice_resultado = busca_sequencial_iterativa (vetor, 10, num);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca sequencial recursiva */
    cout << endl << "-> BUSCA SEQUENCIAL RECURSIVA: ";
    indice_resultado = busca_sequencial_recursiva (vetor, 10, num);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;
    
    vetor_aleatorio_int (vetor, 10);
    quicksort (vetor, 0, 9);
    cout << endl << "Vetor INTEIRO para busca: ";
    imprimi_vetor (vetor, 10);
    cout << endl;

    cout << "Digite um numero para verificar se existe no vetor atraves das funcoes de busca: ";
    cin >> num;
    
    /** Testando a busca binaria recursiva */
    cout << endl << "-> BUSCA BINARIA RECURSIVA: ";
    indice_resultado = busca_binaria_recursiva (vetor, 10, num);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca binaria iterativa */
    cout << endl << "-> BUSCA BINARIA ITERATIVA: ";
    indice_resultado = busca_binaria_iterativa (vetor, 10, num);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca ternaria recursiva */
    cout << endl << "-> BUSCA TERNARIA RECURSIVA: ";
    indice_resultado = busca_ternaria_recursiva (vetor, 0, 9, num);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca ternaria iterativa */
    cout << endl << "-> BUSCA TERNARIA ITERATIVA: ";
    indice_resultado = busca_ternaria_iterativa (vetor, 0, 9, num);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** ********************************  TESTANDO FLOAT   ************************************* */
    cout << endl << "*************************************  TESTANDO FLOAT  *************************************" << endl << endl;    
    vetor_aleatorio_float (vetor_f, 10);
    
    cout << "--> BUSCA EM VETOR DE FLOAT:" << endl << endl;
    cout << "Vetor FLOAT para busca: ";
    imprimi_vetor (vetor_f, 10);
    cout << endl;

    cout << "Digite um numero para verificar se existe no vetor atraves das funcoes de busca: ";
    cin >> num_f;

    /** Testando a busca sequencial iterativa */
    cout << endl << "-> BUSCA SEQUENCIAL ITERATIVA: ";
    indice_resultado = busca_sequencial_iterativa (vetor_f, 10, num_f);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca sequencial recursiva */
    cout << endl << "-> BUSCA SEQUENCIAL RECURSIVA: ";
    indice_resultado = busca_sequencial_recursiva (vetor_f, 10, num_f);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;
    
    vetor_aleatorio_float (vetor_f, 10);
    quicksort (vetor_f, 0, 9);
    cout << endl << "Vetor FLOAT para busca: ";
    imprimi_vetor (vetor_f, 10);
    cout << endl;

    cout << "Digite um numero para verificar se existe no vetor atraves das funcoes de busca: ";
    cin >> num_f;
    
    /** Testando a busca binaria recursiva */
    cout << endl << "-> BUSCA BINARIA RECURSIVA: ";
    indice_resultado = busca_binaria_recursiva (vetor_f, 10, num_f);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca binaria iterativa */
    cout << endl << "-> BUSCA BINARIA ITERATIVA: ";
    indice_resultado = busca_binaria_iterativa (vetor_f, 10, num_f);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca ternaria recursiva */
    cout << endl << "-> BUSCA TERNARIA RECURSIVA: ";
    indice_resultado = busca_ternaria_recursiva (vetor_f, 0, 9, num_f);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca ternaria iterativa */
    cout << endl << "-> BUSCA TERNARIA ITERATIVA: ";
    indice_resultado = busca_ternaria_iterativa (vetor_f, 0, 9, num_f);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** ********************************  TESTANDO STRING   ************************************* */
    cout << endl << "*************************************  TESTANDO STRING  *************************************" << endl << endl;    
    cout << "--> BUSCA EM VETOR DE STRING:" << endl << endl;
    cout << "Vetor STRING para busca: ";
    imprimi_vetor (vetor_s, 10);
    cout << endl;

    string texto;
    cout << "Digite uma string para verificar se existe no vetor atraves das funcoes de busca: ";
    getline (cin >> ws, texto);

    /** Testando a busca sequencial iterativa */
    cout << endl << "-> BUSCA SEQUENCIAL ITERATIVA: ";
    indice_resultado = busca_sequencial_iterativa (vetor_s, 10, texto);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca sequencial recursiva */
    cout << endl << "-> BUSCA SEQUENCIAL RECURSIVA: ";
    indice_resultado = busca_sequencial_recursiva (vetor_s, 10, texto);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;
    
    quicksort (vetor_s, 0, 9);
    cout << endl << "Vetor STRING para busca: ";
    imprimi_vetor (vetor_s, 10);
    cout << endl;

    cout << "Digite um numero para verificar se existe no vetor atraves das funcoes de busca: ";
    getline (cin >> ws, texto);
    
    /** Testando a busca binaria recursiva */
    cout << endl << "-> BUSCA BINARIA RECURSIVA: ";
    indice_resultado = busca_binaria_recursiva (vetor_s, 10, texto);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca binaria iterativa */
    cout << endl << "-> BUSCA BINARIA ITERATIVA: ";
    indice_resultado = busca_binaria_iterativa (vetor_s, 10, texto);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca ternaria recursiva */
    cout << endl << "-> BUSCA TERNARIA RECURSIVA: ";
    indice_resultado = busca_ternaria_recursiva (vetor_s, 0, 9, texto);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;

    /** Testando a busca ternaria iterativa */
    cout << endl << "-> BUSCA TERNARIA ITERATIVA: ";
    indice_resultado = busca_ternaria_iterativa (vetor_s, 0, 9, texto);
    if (indice_resultado != -1)
        cout << "Elemento encontrado no indice " << indice_resultado << " do vetor !!!" << endl;
    else    
        cout << "Elemento nao encontrado !!!" << endl;
}