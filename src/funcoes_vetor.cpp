/**
* @file     funcoes_vetor.cpp
* @brief    Implementacao das funcoes que manipulam um vetor
* @author   Pedro Emerick (p.emerick@live.com)
* @since    20/06/2017
* @date	    20/06/2017
*/

#include "funcoes_vetor.h"

#include <iostream>
using std::cout;
using std::endl;

#include <ctime>

#include <sstream>
using std::stringstream;

#include <string>
using std::string;

/** 
 * @brief	Função que preenche um vetor de inteiros com valores aleatorios de zero a cem
 * @param 	vetor Vetor para inserir os numeros
 * @param	tamanho Tamanho do vetor
 */
void vetor_aleatorio_int (int *vetor, int tamanho)
{
    srand(time(NULL));
    for (int ii = 0; ii < tamanho; ii++)
    {
        vetor[ii] = rand () % 100;
    }
}

/** 
 * @brief	Função que preenche um vetor de floats com valores aleatorios de zero a cem
 * @param 	vetor Vetor para inserir os numeros
 * @param	tamanho Tamanho do vetor
 */
void vetor_aleatorio_float (float *vetor, int tamanho)
{
    srand(time(NULL));
    for (int ii = 0; ii < tamanho; ii++)
    {
        vetor[ii] = rand () % 100;
        int aux = rand () % 100;
        vetor[ii] += (float) aux/1000;
    }
}

/** 
 * @brief	Função que preenche um vetor de strings com valores aleatorios de zero a cem
 * @param 	vetor Vetor para inserir as strings
 * @param	tamanho Tamanho do vetor
 */
void vetor_aleatorio_string (string *vetor, int tamanho)
{
    srand(time(NULL));
    for (int ii = 0; ii < tamanho; ii++)
    {
        vetor[ii] = "teste";

        int aux = rand () % 30;        
        stringstream num_teste;
        num_teste << aux;

        vetor[ii] += num_teste.str ();
    }
}