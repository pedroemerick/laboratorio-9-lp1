/**
* @file     teste_ordenacao.cpp
* @brief    Implementacao da funcao que faz os testes com os algoritmos de ordenacao
* @author   Pedro Emerick (p.emerick@live.com)
* @since    17/06/2017
* @date	    20/06/2017
*/

#include "teste_ordenacao.h"

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include "quicksort.h"
#include "mergesort.h"
#include "bubllesort.h"
#include "insertionsort.h"
#include "selectionsort.h"

#include "funcoes_vetor.h"

using namespace edb1;

/** 
 * @brief	Função que realiza testes nos algoritmos de ordenacao, implementados na biblioteca
 */
void testes_ordenacao ()
{
    cout << "O vetor usado nestes testes tem tamanho 10 com valores aleatorios";
    cout << endl << endl;

    int vetor [10];
    float vetor_f [10];
    string vetor_s [10];

    cout << "***********************************" << endl;
    cout << "*       TESTANDO ORDENACOES       *" << endl;
    cout << "***********************************" << endl << endl;

    /** Testando o quicksort */
    vetor_aleatorio_int (vetor, 10);
    cout << "-> QUICKSORT" << endl << endl;
    cout << "Vetor de inteiro antes da ordenacao: ";
    imprimi_vetor (vetor, 10);
    quicksort (vetor, 0, 9);
    cout << "Vetor de inteiro depois da ordenacao: ";
    imprimi_vetor (vetor, 10);

    vetor_aleatorio_float (vetor_f, 10);
    cout << endl << "Vetor de float antes da ordenacao: ";
    imprimi_vetor (vetor_f, 10);
    quicksort (vetor_f, 0, 9);
    cout << "Vetor de float depois da ordenacao: ";
    imprimi_vetor (vetor_f, 10);

    vetor_aleatorio_string (vetor_s, 10);
    cout << endl << "Vetor de string antes da ordenacao: ";
    imprimi_vetor (vetor_s, 10);
    quicksort (vetor_s, 0, 9);
    cout << "Vetor de string depois da ordenacao: ";
    imprimi_vetor (vetor_s, 10);

    /** Testando o insertionsort */
    vetor_aleatorio_int (vetor, 10);
    cout << endl << "-> INSERTIONSORT" << endl << endl;
    cout << "Vetor de inteiro antes da ordenacao: ";
    imprimi_vetor (vetor, 10);
    insertionsort (vetor, 10);
    cout << "Vetor de inteiro depois da ordenacao: ";
    imprimi_vetor (vetor, 10);

    vetor_aleatorio_float (vetor_f, 10);
    cout << endl << "Vetor de float antes da ordenacao: ";
    imprimi_vetor (vetor_f, 10);
    insertionsort (vetor_f, 10);
    cout << "Vetor de float depois da ordenacao: ";
    imprimi_vetor (vetor_f, 10);

    vetor_aleatorio_string (vetor_s, 10);
    cout << endl << "Vetor de string antes da ordenacao: ";
    imprimi_vetor (vetor_s, 10);
    insertionsort (vetor_s, 10);
    cout << "Vetor de string depois da ordenacao: ";
    imprimi_vetor (vetor_s, 10);

    /** Testando o bubllesort */
    vetor_aleatorio_int (vetor, 10);
    cout << endl << "-> BUBLLESORT" << endl << endl;
    cout << "Vetor de inteiro antes da ordenacao: ";
    imprimi_vetor (vetor, 10);
    bubllesort (vetor, 10);
    cout << "Vetor de inteiro depois da ordenacao: ";
    imprimi_vetor (vetor, 10);

    vetor_aleatorio_float (vetor_f, 10);
    cout << endl << "Vetor de float antes da ordenacao: ";
    imprimi_vetor (vetor_f, 10);
    bubllesort (vetor_f, 10);
    cout << "Vetor de float depois da ordenacao: ";
    imprimi_vetor (vetor_f, 10);

    vetor_aleatorio_string (vetor_s, 10);
    cout << endl << "Vetor de string antes da ordenacao: ";
    imprimi_vetor (vetor_s, 10);
    bubllesort (vetor_s, 10);
    cout << "Vetor de string depois da ordenacao: ";
    imprimi_vetor (vetor_s, 10);
  
    /** Testando o mergesort */
    vetor_aleatorio_int (vetor, 10);
    cout << endl << "-> MERGESORT" << endl << endl;
    cout << "Vetor de inteiro antes da ordenacao: ";
    imprimi_vetor (vetor, 10);
    mergesort (vetor, 0, 9);
    cout << "Vetor de inteiro depois da ordenacao: ";
    imprimi_vetor (vetor, 10);

    vetor_aleatorio_float (vetor_f, 10);
    cout << endl << "Vetor de float antes da ordenacao: ";
    imprimi_vetor (vetor_f, 10);
    mergesort (vetor_f, 0, 9);
    cout << "Vetor de float depois da ordenacao: ";
    imprimi_vetor (vetor_f, 10);

    vetor_aleatorio_string (vetor_s, 10);
    cout << endl << "Vetor de string antes da ordenacao: ";
    imprimi_vetor (vetor_s, 10);
    mergesort (vetor_s, 0, 9);
    cout << "Vetor de string depois da ordenacao: ";
    imprimi_vetor (vetor_s, 10);
  
    /** Testando o selectionsort */
    vetor_aleatorio_int (vetor, 10);
    cout << endl << "-> SELECTIONSORT" << endl << endl;
    cout << "Vetor de inteiro antes da ordenacao: ";
    imprimi_vetor (vetor, 10);
    selectionsort (vetor, 10);
    cout << "Vetor de inteiro depois da ordenacao: ";
    imprimi_vetor (vetor, 10);

    vetor_aleatorio_float (vetor_f, 10);
    cout << endl << "Vetor de float antes da ordenacao: ";
    imprimi_vetor (vetor_f, 10);
    selectionsort (vetor_f, 10);
    cout << "Vetor de float depois da ordenacao: ";
    imprimi_vetor (vetor_f, 10);

    vetor_aleatorio_string (vetor_s, 10);
    cout << endl << "Vetor de string antes da ordenacao: ";
    imprimi_vetor (vetor_s, 10);
    selectionsort (vetor_s, 10);
    cout << "Vetor de string depois da ordenacao: ";
    imprimi_vetor (vetor_s, 10);
}