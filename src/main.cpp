/**
* @file	    main.cpp
* @brief	Arquivo com a função principal do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since	17/06/2017
* @date	    20/06/2017
*/

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include "teste_ordenacao.h"
#include "teste_tads.h"
#include "teste_busca.h"

/**
* @brief Função principal do programa
*/
int main () 
{
    int opcao = -1;

    while (opcao != 0)
    {
        cout << endl;
        cout << "*****************************************************" << endl;
        cout << "*                   Menu de Testes                  *" << endl;
        cout << "*                                                   *" << endl;
        cout << "*      1) Testar Ordenacoes                         *" << endl;
        cout << "*      2) Testar TADs                               *" << endl;
        cout << "*      3) Testar Buscas                             *" << endl;
        cout << "*                                                   *" << endl;
        cout << "*      0) Sair                                      *" << endl;
        cout << "*                                                   *" << endl;
        cout << "*****************************************************" << endl << endl;

        cout << "Digite a opcao que deseja:" << endl << "--> ";
        cin >> opcao;
        cout << endl;

        switch (opcao)
        {
            case 1:
                testes_ordenacao ();
                break;
            case 2:
                testes_tads ();
                break;
            case 3:
                testes_busca ();
                break;
            case 0:
                break;
            default:
                cout << "Opcao invalida !!!" << endl;
                break;
        }
    }
    
    return 0;
}