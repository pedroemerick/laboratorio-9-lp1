/**
* @file	    lib.cpp
* @brief	Arquivo apenas com as inclusoes para gerar as bibliotecas
* @author   Pedro Emerick (p.emerick@live.com)
* @since	19/06/2017
* @date	    20/06/2017
*/

#include <iostream>

#include "pilha.h"
#include "ll_dupla_ord.h"
#include "fila.h"
#include "buscas.h"
#include "_node.h"
#include "quicksort.h"
#include "mergesort.h"
#include "bubllesort.h"
#include "insertionsort.h"
#include "selectionsort.h"

/**
* @brief Função para geração das bibliotecas
*/
int main () {

    return 0;
}
